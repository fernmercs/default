package edu.ucmerced.fern;

import java.util.Vector;

/**
 * Created by Eyob on 3/29/2016.
 */
public class Term {
    //A semester/quarter that is part of a four(x)-year schedule. When a schedule is some units away from completion, a new term is made to encompass courses.
    Vector<Course> classesForTerm;

    public Term(){
        classesForTerm = new Vector<Course>(0,2);

    }
}
