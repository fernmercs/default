package edu.ucmerced.fern;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class MajorsOffered extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_major_list);

        Spinner list =(Spinner) findViewById(R.id.catyear);
        ArrayAdapter auu = ArrayAdapter.createFromResource(this, R.array.cat_year_list, android.R.layout.simple_spinner_item);

        auu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        list.setAdapter(auu);

        //Fragment tests
       /* FragmentManager fM= getFragmentManager();
        FragmentTransaction fT = fM.beginTransaction();

        major frag = new major();
        fT.add(R.id.majorList, frag);
        fT.commit();
        setTitle("Oh");
*/

        /*FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        DetailsFragment newFrag = DetailsFragment.newInstance();

        ft.replace(R.id.majorList,newFrag,"Major");*/

        //Autopopulate list of majors as buttons [take from school year and school location database]
    }

}
