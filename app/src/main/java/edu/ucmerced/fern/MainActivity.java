package edu.ucmerced.fern;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);//Only important one
        final Handler handler = new Handler();

        this.setContentView(R.layout.screen_loading);
        ImageView iv=(ImageView) findViewById(R.id.load);
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
       iv.setImageResource(R.drawable.loadscreentest);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
    homePage();
            }
        }, 3000);
    }

    protected void homePage(){
        setContentView(R.layout.activity_main);
        // getSupportActionBar().setTitle("Hello from the Other Side");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent majorList = new Intent(getBaseContext(), MajorsOffered.class);
                startActivity(majorList);
            }
        });

        final SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.savedlist), Context.MODE_PRIVATE);

        //Go to login page if there isn't even a reference to how many class plans exist.
        //Can comment out until deployment build
        /*
        if(!sharedPref.contains("plan_count")){
            Intent login = new Intent(this, icing.class);
            startActivity(login);
        }
        */

        if(!sharedPref.contains("plan_count")){
            planInit(sharedPref);
        }

        printButtons(sharedPref);

    }
    public boolean printButtons(SharedPreferences sharedPref) {
        List<Schedule> Schedlist;
        String buttonID;
        if(!sharedPref.contains("plan_count") || sharedPref.getInt("plan_count",0) < 1) {
            Schedlist = new ArrayList<Schedule>(); //Maybe use to contain list of schedules (will update and get from a file containing this list)
        } else{
            //Find out how to store and load these Schedule objects
        }
        //get number of buttons to print
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.mainContent);
        Button planButton = new Button(getApplicationContext());

        // int buttonNum = sharedPref.getInt("plan_count", 0);
        // for(int i = 0; i < buttonNum; i++){}
        //int placeholderID=0; change this to a linear ID used to place buttons below each other/currently, only working value is zero
        planButton.setTextSize(14);
        planButton.setText("Select taken courses");
       // planButton.setText("Refresh main page");
        planButton.setId(generateViewId());
        planButton.setBackgroundColor(0xFFA21E31);
        //Use onClickListener to take user to page with schedule displayed
        planButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Will use to take user to page with schedule displayed", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
               /// / finish();
               // startActivity(getIntent());
                Intent test = new Intent(getApplicationContext(), RegisteredCourses.class);
                startActivity(test);
            }
        });

        RelativeLayout.LayoutParams meh = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);


        meh.addRule(RelativeLayout.CENTER_HORIZONTAL); //Puts it in middle of screen horizontally


        //Use array of ids that get assigned via planButton.setID()s
        meh.addRule(RelativeLayout.BELOW, R.id.button); //Puts it underneath the default button

        planButton.setLayoutParams(meh);
        layout.addView(planButton);

        //Try to create a series of buttons from here.
        Vector<Button> heynow = new Vector<Button>(0,1);
        heynow.add(planButton);
        for(int hey = 0; hey<0; hey++){ //disable buttons

            planButton = new Button(getApplicationContext());
            planButton.setTextSize(14);
            planButton.setText("placeholder " + Integer.toString(hey));
            planButton.setId(generateViewId());
            //Use onClickListener to take user to page with schedule displayed
            planButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "I am a programmatically created button." , Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
/*
           meh = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);


        meh.addRule(RelativeLayout.CENTER_HORIZONTAL);
            //Use array of ids that get assigned via planButton.setID()s
            meh.addRule(RelativeLayout.BELOW, heynow.get(hey).getId()); //Puts it underneath the default button

            planButton.setLayoutParams(meh);
            //planButton.setBackgroundColor(getResources().getColor(R.color.Anna2, getTheme()));
            layout.addView(planButton);
            heynow.add(planButton);
            */
        }

        return true;
    }

    public boolean planInit(SharedPreferences sharedPref){
        //Create default number of plans/schedules (0)
        sharedPref.edit().putInt("plan_count", 0);

        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Obtained from stackoverflow
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }
}
