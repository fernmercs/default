package edu.ucmerced.fern;

import java.util.Vector;

/**
 * Created by Eyob on 3/9/2016.
 */
public class Schedule {

    String name;
    int units;
    String major;
    int year;
    int unitstoGrad; //Number of generic units required to fill graduation quota
    Vector<Term> terms;

    public Schedule(){ //No argument constructor
        name = "";
        units = 0;
        year = 2000;
        major = "";
        unitstoGrad = 0;
        terms = new Vector<Term>(0,1);
    }

    public void makeDefName(){
        name = major +" '" + Integer.toString(year).substring(2,4);
    }

    //Create a custom name for the schedule given an input
    public void makeCusName(String customName){
        name = customName;
    }


    public String getName() {
        return name;
    }

    public int getUnits() {

        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getUnitstoGrad() {
        return unitstoGrad;
    }

    public void setUnitstoGrad(int unitstoGrad) {
        this.unitstoGrad = unitstoGrad;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public void Generate(){} // Create schedules to display

}
