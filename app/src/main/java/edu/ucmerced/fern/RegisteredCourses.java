package edu.ucmerced.fern;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

public class RegisteredCourses extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered_courses);
        String[] arr = {"Placeholder"};
     //   DATABASE("SELECT DISTINCT c_subject FROM Course","c_subject" , arr);
        String[] x = DATABASE("SELECT DISTINCT c_subject FROM Course","c_subject" , arr);
        Log.d("db", "array's length is " + x.length);
        printButtons(x);
        Log.d("db", "arr saved");
    }


    public String[] DATABASE(String q, String index, String[] arr){
        //DATABASE
        Log.d("db", "Starting database method.");
        String DB_PATH = "data/data/edu.ucmerced.fern/"; // path
        String DBNAME = "Fern.db";
        String outFileName = DB_PATH + DBNAME;
        try {
            InputStream is = getAssets().open(DBNAME);

            Log.d("db", "database file exists: " + is.available());


            OutputStream myOutput = new FileOutputStream(outFileName);

//transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

//Close the streams
            myOutput.flush();
            myOutput.close();
            is.close();
            is = new FileInputStream(DB_PATH + DBNAME);
            Log.d("db", "output database file exists: " + is.available());
            is.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //SQLiteDatabase db = mDBHelper.getReadableDatabase();
        SQLiteDatabase db = SQLiteDatabase.openDatabase(DB_PATH+DBNAME, null, SQLiteDatabase.OPEN_READONLY);

        ArrayList<String> arrTblNames = new ArrayList<String>();
        //Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        Cursor c = db.rawQuery(q, null);

        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                arrTblNames.add( c.getString( c.getColumnIndex(index)) );
                c.moveToNext();
            }
        }

        arr = new String[arrTblNames.size()];
        arrTblNames.toArray(arr);
        for(int x=0; x<arrTblNames.size();x++){
            Log.d("db", "Table " + (x+1) + " is " + arr[x]);
        }
        //This is all that's necessary for now


/*
        String[] projection = {
                "m_Nickname"
        };
        c = db.query("Mentor", projection, null, null, null,null, null);

        arrTblNames = new ArrayList<String>();

        int itemId;
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                arrTblNames.add( c.getString(c.getInt(
                        c.getColumnIndexOrThrow(projection[0])
                )) );
                c.moveToNext();
            }
        }
        arr = new String[arrTblNames.size()];
        arrTblNames.toArray(arr);
        for(int x=0; x<arrTblNames.size();x++){
            Log.d("db", "Name " + (x + 1) + " is " + arr[x]);
        }
*/
        db.close();
        return arr;
    }

    public boolean printButtons(String[] arr) {

        String buttonID;

        //get number of buttons to print
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.SubList);
       Button planButton = new Button(getApplicationContext());

        // int buttonNum = sharedPref.getInt("plan_count", 0);
        // for(int i = 0; i < buttonNum; i++){}
        //int placeholderID=0; change this to a linear ID used to place buttons below each other/currently, only working value is zero
        planButton.setTextSize(14);
        planButton.setText(arr[0]);
        // planButton.setText("Refresh main page");
        planButton.setId(generateViewId());
        planButton.setBackgroundColor(0xFFA21E31);
        //Use onClickListener to take user to page with schedule displayed
        planButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Will use to take user to page with schedule displayed", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                /// / finish();
                // startActivity(getIntent());
                //Intent test = new Intent(getApplicationContext(), RegisteredCourses.class);
                //startActivity(test);
            }
        });
        Log.d("db", "Made first button");
        RelativeLayout.LayoutParams meh = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);


        meh.addRule(RelativeLayout.CENTER_HORIZONTAL); //Puts it in middle of screen horizontally


        //Use array of ids that get assigned via planButton.setID()s
        meh.addRule(RelativeLayout.BELOW, R.id.header); //Puts it underneath the default button

        planButton.setLayoutParams(meh);
        layout.addView(planButton);
        Log.d("db", "Added first button with text " + planButton.getText());
        //Try to create a series of buttons from here.
        Vector<Button> heynow = new Vector<Button>(0,1);
        heynow.add(planButton);
        for(int hey = 1; hey<arr.length; hey++){ //disable buttons

            planButton = new Button(getApplicationContext());
            planButton.setTextSize(14);
            planButton.setText(arr[hey]);

            planButton.setBackgroundColor(0xFFA21E31);
            planButton.setId(generateViewId());
            //Use onClickListener to take user to page with schedule displayed
            final String text = arr[hey];
            planButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Snackbar.make(view, "I am a programmatically created button.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/
                    Intent test = new Intent(getApplicationContext(), CourseList.class);
                    Bundle er = new Bundle();
                    er.putString("subject", text);
                    test.putExtras(er);
                    startActivity(test);
                }
            });
            Log.d("db", "Made button " + hey +" with text " +planButton.getText() );
           meh = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        meh.addRule(RelativeLayout.CENTER_HORIZONTAL);
            //Use array of ids that get assigned via planButton.setID()s
            meh.addRule(RelativeLayout.BELOW, heynow.get(hey-1).getId()); //Puts it underneath the default button
            Log.d("db", "Set rules");
            planButton.setLayoutParams(meh);
            //planButton.setBackgroundColor(getResources().getColor(R.color.Anna2, getTheme()));
            Log.d("db", "About to add button");
            layout.addView(planButton);
            heynow.add(planButton);
            Log.d("db", "Added button " + hey+1);

        }

        return true;
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }
}
