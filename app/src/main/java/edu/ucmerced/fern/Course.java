package edu.ucmerced.fern;

import java.util.Vector;

/**
 * Created by Eyob on 3/28/2016.
 */
public class Course {

    String name;
    int units;
    String description;
    Vector<Course> prereqs;

    public Course() {
        name = "BLNK 000";//May change this to two variables [subject] + [course number]
        units = 4; //Most classes are four units at UC Merced; this should usually be changed
        description="I'm a class!";
        prereqs = new Vector<Course>(0,1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vector<Course> getPrereqs() {
        return prereqs;
    }

    public void setPrereqs(Vector<Course> prereqs) {
        this.prereqs = prereqs;
    }
}
